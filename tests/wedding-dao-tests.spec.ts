import { dbClient } from "../src/connection";
import ExpenseDao from "../src/daos/expense-dao";
import ExpenseDaoImpl from "../src/daos/expense-dao-impl";
import WeddingDao from "../src/daos/wedding-dao";
import WeddingDaoImpl from "../src/daos/wedding-dao-impl";
import { Expense, Wedding } from "../src/entities";

const weddingDao:WeddingDao = new WeddingDaoImpl();
const expenseDao:ExpenseDao = new ExpenseDaoImpl();

test("Create Wedding", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 1", 10000);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
});

test("Get wedding by ID", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 2", 10000);
    wedding = await weddingDao.createWedding(wedding);
    //checking that wedding was made
    expect(wedding.weddingId).not.toBe(0);
    let testWedding:Wedding = await weddingDao.getWedding(wedding.weddingId);
    //checking that added wedding equals retrieved wedding
    expect(testWedding.name).toBe(wedding.name);
});

test("Get all weddings", async ()=>{
    let wedding1:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 3-1", 10000);
    let wedding2:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 3-2", 10000);
    let wedding3:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 3-3", 10000);
    wedding1 = await weddingDao.createWedding(wedding1);
    wedding2 = await weddingDao.createWedding(wedding2);
    wedding3 = await weddingDao.createWedding(wedding3);
    expect(wedding1.weddingId).not.toBe(0);
    expect(wedding2.weddingId).not.toBe(0);
    expect(wedding3.weddingId).not.toBe(0);
    let allWeddings:Wedding[] = await weddingDao.getAllWeddings();
    expect(allWeddings.filter(w => w.weddingId === wedding1.weddingId)[0].name).toBe(wedding1.name);
    expect(allWeddings.filter(w => w.weddingId === wedding2.weddingId)[0].name).toBe(wedding2.name);
    expect(allWeddings.filter(w => w.weddingId === wedding3.weddingId)[0].name).toBe(wedding3.name);
});

test("Update Wedding", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 4", 100);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    wedding.name="Wedding Test 4 Update";
    wedding = await weddingDao.updateWedding(wedding);
    let testWedding:Wedding = await weddingDao.getWedding(wedding.weddingId);
    expect (testWedding.name).toBe(wedding.name);
});

test("Delete Wedding", async () =>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 5", 100);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    const deleted:boolean = await weddingDao.deleteWedding(wedding.weddingId);
    expect(deleted).toBe(true);
    try{
        let testWedding:Wedding = await weddingDao.getWedding(wedding.weddingId);
        //Delete failed if this line is reached withtout throwing an error
        expect(1).toBe(0);
    }catch(error){
        expect(error.message).toBe(`Error: Wedding with id ${wedding.weddingId} not found`);
    }
});

test("Illegal Delete", async () => {
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Wedding Test 5", 100);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    let expense:Expense = new Expense(0, wedding.weddingId, "Expense Test 1", 5000);
    expense = await expenseDao.createExpense(expense);
    expect(expense.expenseId).not.toBe(0);

    try{
        const deleted = await weddingDao.deleteWedding(wedding.weddingId);
        //line should throw error
        expect(1).toBe(0);
    }catch(error){
        expect(error.message).toBe(`update or delete on table \"wedding\" violates foreign key constraint \"fk_expense_wedding\" on table \"expense\"`);
    }

    let deleted = await expenseDao.deleteExpense(expense.expenseId);
    expect(deleted).toBe(true);
    deleted = await weddingDao.deleteWedding(wedding.weddingId);
    expect(deleted).toBe(true);
});

afterAll(async ()=>{
    dbClient.end();
});