import { dbClient } from "../src/connection";
import ExpenseDao from "../src/daos/expense-dao";
import ExpenseDaoImpl from "../src/daos/expense-dao-impl";
import WeddingDao from "../src/daos/wedding-dao";
import WeddingDaoImpl from "../src/daos/wedding-dao-impl";
import { Wedding, Expense } from "../src/entities";

const weddingDao:WeddingDao = new WeddingDaoImpl();
const expenseDao:ExpenseDao = new ExpenseDaoImpl();

test("Create Expense", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 1", 10000);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    let expense:Expense = new Expense(0, wedding.weddingId, "Expense Test 1", 5000);
    expense = await expenseDao.createExpense(expense);
    expect(expense.expenseId).not.toBe(0);
});

test("Get Expense", async ()=>{
    //generate expense
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 2", 10000);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    let expense:Expense = new Expense(0, wedding.weddingId, "Expense Test 2", 5000);
    expense = await expenseDao.createExpense(expense);
    expect(expense.expenseId).not.toBe(0);
    //getting expense by id
    const testExpense:Expense = await expenseDao.getExpense(expense.expenseId);
    expect(testExpense.reason).toBe(expense.reason);
});

test("Get All Expenses", async ()=>{
    let wedding1:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 3-1", 10000);
    let wedding2:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 3-2", 10000);
    wedding1 = await weddingDao.createWedding(wedding1);
    wedding2 = await weddingDao.createWedding(wedding2);
    expect(wedding1.weddingId).not.toBe(0);
    expect(wedding2.weddingId).not.toBe(0);
    let expense1:Expense = new Expense(0, wedding1.weddingId, "Expense Test 3-1", 5000);
    let expense2:Expense = new Expense(0, wedding2.weddingId, "Expense Test 3-1", 5000);
    expense1 = await expenseDao.createExpense(expense1);
    expense2 = await expenseDao.createExpense(expense2);
    expect(expense1.expenseId).not.toBe(0);
    expect(expense2.expenseId).not.toBe(0);
    //getting expense by id
    const allExpenses:Expense[] = await expenseDao.getAllExpenses();
    expect(allExpenses.filter(e => e.expenseId === expense1.expenseId)[0].reason).toBe(expense1.reason);
    expect(allExpenses.filter(e => e.expenseId === expense2.expenseId)[0].reason).toBe(expense2.reason);
});

test("Get All Wedding Expenses", async ()=>{
    //Creating expenses for 2 weddings and making sure that only expenses for wedding 1
    //are returned for getting expenses for that wedding
    let wedding1:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 4-1", 20000);
    let wedding2:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 4-2", 20000);
    wedding1 = await weddingDao.createWedding(wedding1);
    wedding2 = await weddingDao.createWedding(wedding2);
    expect(wedding1.weddingId).not.toBe(0);
    expect(wedding2.weddingId).not.toBe(0);
    let expense1:Expense = new Expense(0, wedding1.weddingId, "Expense Test 4-1", 5000);
    let expense2:Expense = new Expense(0, wedding1.weddingId, "Expense Test 4-2", 5000);
    let expense3:Expense = new Expense(0, wedding1.weddingId, "Expense Test 4-3", 5000);
    let expense4:Expense = new Expense(0, wedding2.weddingId, "Expense Test 4-4", 5000);
    expense1 = await expenseDao.createExpense(expense1);
    expense2 = await expenseDao.createExpense(expense2);
    expense3 = await expenseDao.createExpense(expense3);
    expense4 = await expenseDao.createExpense(expense4);
    expect(expense1.expenseId).not.toBe(0);
    expect(expense2.expenseId).not.toBe(0);
    expect(expense3.expenseId).not.toBe(0);
    expect(expense4.expenseId).not.toBe(0);

    const weddingExpenses:Expense[] = await expenseDao.getAllWeddingExpenses(wedding1.weddingId);

    expect(weddingExpenses.filter(e => e.expenseId === expense1.expenseId)[0].reason).toBe(expense1.reason);
    expect(weddingExpenses.filter(e => e.expenseId === expense2.expenseId)[0].reason).toBe(expense2.reason);
    expect(weddingExpenses.filter(e => e.expenseId === expense3.expenseId)[0].reason).toBe(expense3.reason);
    expect(weddingExpenses.filter(e => e.expenseId === expense4.expenseId).length).toBe(0);
});

test("Update Expense", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 5", 10000);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    let expense:Expense = new Expense(0, wedding.weddingId, "Expense Test 5", 5000);
    expense = await expenseDao.createExpense(expense);
    expect(expense.expenseId).not.toBe(0);

    expense.reason="Expense Test 5 Update";
    expense = await expenseDao.updateExpense(expense);
    const testExpense:Expense = await expenseDao.getExpense(expense.expenseId);
    expect(expense.reason).toBe(testExpense.reason);
});

test("Delete Expense", async ()=>{
    let wedding:Wedding = new Wedding(0, "0000-00-00", "Pittsburgh", "Expense Test 1", 10000);
    wedding = await weddingDao.createWedding(wedding);
    expect(wedding.weddingId).not.toBe(0);
    let expense:Expense = new Expense(0, wedding.weddingId, "Expense Test 1", 5000);
    expense = await expenseDao.createExpense(expense);
    expect(expense.expenseId).not.toBe(0);
    
    let deleted:boolean = await expenseDao.deleteExpense(expense.expenseId);
    expect(deleted).toBe(true);

    try{
        const testExpense:Expense = await expenseDao.getExpense(expense.expenseId);
        //above line should throw an error
        expect(true).toBe(false);
    }catch(error){
        expect(error.message).toBe(`Error: Expense with id ${expense.expenseId} not found`);
    }
});

afterAll(async ()=>{
    dbClient.end();
});