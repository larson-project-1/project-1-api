import { dbClient } from "../connection";
import { Wedding } from "../entities";
import { ResourceNotFoundError } from "../errors";
import WeddingDao from "./wedding-dao";

export default class WeddingDaoImpl implements WeddingDao{
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding (wedding_date,wedding_location,wedding_name,wedding_budget) values ($1,$2,$3,$4) returning wedding_id"
        const values = [wedding.date, wedding.location, wedding.name, wedding.budget];
        const result = await dbClient.query(sql, values);
        wedding.weddingId = result.rows[0].wedding_id;
        return wedding;
    }
    async getWedding(weddingId: number): Promise<Wedding> {
        const sql:string = "select * from wedding where wedding_id=$1"
        const values = [weddingId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Wedding with id ${weddingId} not found`);
        }
        return result.rows.map(w => new Wedding(w.wedding_id, w.wedding_date, w.wedding_location, w.wedding_name, w.wedding_budget))[0];
    }

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = "select * from wedding"
        const result = await dbClient.query(sql);
        return result.rows.map(w => new Wedding(w.wedding_id, w.wedding_date, w.wedding_location, w.wedding_name, w.wedding_budget));
    }

    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wedding_date=$1,wedding_location=$2,wedding_name=$3,wedding_budget=$4 where wedding_id=$5';
        const values:any[] = [wedding.date, wedding.location, wedding.name, wedding.budget, wedding.weddingId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Wedding with id ${wedding.weddingId} not found`)
        }
        return wedding;
    }

    async deleteWedding(weddingId: number): Promise<boolean> {
        const sql:string = 'delete from wedding where wedding_id = $1';
        const values:any[] = [weddingId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Wedding with id ${weddingId} not found`);
        }
        return true;
    }
}