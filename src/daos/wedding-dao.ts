import { Wedding } from "../entities";

export default interface WeddingDao{
    createWedding(wedding:Wedding):Promise<Wedding>;

    getWedding(wedding_id:number):Promise<Wedding>;

    getAllWeddings():Promise<Wedding[]>;

    updateWedding(wedding:Wedding):Promise<Wedding>;
    
    deleteWedding(wedding_id:number):Promise<boolean>;
}