import { dbClient } from "../connection";
import { Expense } from "../entities";
import { ResourceNotFoundError } from "../errors";
import ExpenseDao from "./expense-dao";

export default class ExpenseDaoImpl implements ExpenseDao{
    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into expense (reason,amount,w_id) values ($1,$2,$3) returning expense_id"
        const values = [expense.reason, expense.amount, expense.wId];
        const result = await dbClient.query(sql, values);
        expense.expenseId = result.rows[0].expense_id;
        return expense;
    }
    async getExpense(expenseId: number): Promise<Expense> {
        const sql:string = "select * from expense where expense_id=$1"
        const values = [expenseId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Expense with id ${expenseId} not found`);
        }
        return result.rows.map(e => new Expense(e.expense_id, e.w_id, e.reason, e.amount))[0];
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = "select * from expense";
        const result = await dbClient.query(sql);
        return result.rows.map(e => new Expense(e.expense_id, e.w_id, e.reason, e.amount));
    }

    async getAllWeddingExpenses(wedding_id: number): Promise<Expense[]> {
        const sql:string = "select * from expense where w_id=$1"
        const values = [wedding_id];
        const result = await dbClient.query(sql, values);
        return result.rows.map(e => new Expense(e.expense_id, e.w_id, e.reason, e.amount));
    }

    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set reason=$1,amount=$2,w_id=$3 where expense_id=$4';
        const values:any[] = [expense.reason, expense.amount, expense.wId, expense.expenseId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Expense with id ${expense.expenseId} not found`);
        }
        return expense;
    }
    async deleteExpense(expenseId: number): Promise<boolean> {
        const sql:string = 'delete from expense where expense_id = $1';
        const values:any[] = [expenseId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Expense with id ${expenseId} not found`);
        }
        return true;
    }

}