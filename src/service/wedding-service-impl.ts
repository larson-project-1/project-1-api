import WeddingDao from "../daos/wedding-dao";
import WeddingDaoImpl from "../daos/wedding-dao-impl";
import { Wedding } from "../entities";
import WeddingService from "./wedding-service";

const weddingDao:WeddingDao = new WeddingDaoImpl();

export default class WeddingServiceImpl implements WeddingService{
    createWedding(wedding: Wedding): Promise<Wedding> {
        return weddingDao.createWedding(wedding);
    }
    getWedding(weddingId: number): Promise<Wedding> {
        return weddingDao.getWedding(weddingId);
    }
    getAllWeddings(): Promise<Wedding[]> {
        return weddingDao.getAllWeddings();
    }
    updateWedding(wedding: Wedding): Promise<Wedding> {
        return weddingDao.updateWedding(wedding);
    }
    deleteWedding(weddingId: number): Promise<boolean> {
        return weddingDao.deleteWedding(weddingId);
    }
    
}