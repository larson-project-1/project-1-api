import { Expense } from "../entities";

export default interface ExpenseService{
    createExpense(expense:Expense):Promise<Expense>;

    getExpense(expenseId:number):Promise<Expense>;

    getAllExpenses():Promise<Expense[]>;

    getAllWeddingExpenses(weddingId:number):Promise<Expense[]>;

    updateExpense(expense:Expense):Promise<Expense>;
    
    deleteExpense(expenseId:number):Promise<boolean>;
}