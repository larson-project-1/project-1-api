import ExpenseDao from "../daos/expense-dao";
import ExpenseDaoImpl from "../daos/expense-dao-impl";
import { Expense } from "../entities";
import ExpenseService from "./expense-service";

const expenseDao:ExpenseDao = new ExpenseDaoImpl();

export default class ExpenseServiceImpl implements ExpenseService{
    createExpense(expense: Expense): Promise<Expense> {
        return expenseDao.createExpense(expense);
    }
    getExpense(expenseId: number): Promise<Expense> {
        return expenseDao.getExpense(expenseId);
    }
    getAllExpenses(): Promise<Expense[]> {
        return expenseDao.getAllExpenses();
    }
    getAllWeddingExpenses(wedding_id: number): Promise<Expense[]> {
        return expenseDao.getAllWeddingExpenses(wedding_id);
    }
    updateExpense(expense: Expense): Promise<Expense> {
        return expenseDao.updateExpense(expense);
    }
    deleteExpense(expenseId: number): Promise<boolean> {
        return expenseDao.deleteExpense(expenseId);
    }
    
}