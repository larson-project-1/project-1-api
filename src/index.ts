import cors from "cors";
import express, { application } from "express";
import { Expense, Wedding } from "./entities";
import ExpenseService from "./service/expense-service";
import ExpenseServiceImpl from "./service/expense-service-impl";
import WeddingService from "./service/wedding-service";
import WeddingServiceImpl from "./service/wedding-service-impl";

const app = express();
app.use(express.json());
app.use(cors());

const port = 3000;

app.listen(port, ()=>{
    console.log(`Server started on port ${port}`);
});

const weddingServie:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

app.get("/", (req, res) => {
    res.send("Wedding Planning!");
});

//create wedding
app.post("/weddings", async (req,res) =>{
    let wedding:Wedding = req.body;
    wedding = await weddingServie.createWedding(wedding);
    res.status(201);
    res.send(wedding);
});

//get all weddings
app.get("/weddings", async (req,res) =>{
    let weddings:Wedding[] = await weddingServie.getAllWeddings();
    res.status(200);
    res.send(weddings);
});

//get wedding by id
app.get("/weddings/:weddingId", async (req,res) =>{
    const weddingId:number = parseInt(req.params.weddingId);
    try{
        let wedding:Wedding = await weddingServie.getWedding(weddingId);
        res.status(200);
        res.send(wedding);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//update wedding
app.put("/weddings/:weddingId", async (req,res) =>{
    const weddingId:number = parseInt(req.params.weddingId);
    let wedding:Wedding = req.body;
    wedding.weddingId = weddingId;
    try{
        wedding = await weddingServie.updateWedding(wedding);
        res.status(200);
        res.send(wedding);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//delete wedding
app.delete("/weddings/:weddingId", async (req,res) =>{
    const weddingId:number = parseInt(req.params.weddingId);
    try{
        let deleted:boolean = await weddingServie.deleteWedding(weddingId);
        res.status(200);
        console.log(deleted);
        res.send(deleted);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//get all expenses for wedding
app.get("/weddings/:weddingId/expenses", async (req,res) =>{
    const weddingId:number = parseInt(req.params.weddingId);
    try{
        const expenses:Expense[] = await expenseService.getAllWeddingExpenses(weddingId);
        res.status(200);
        res.send(expenses);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//create expense
app.post("/expenses", async (req,res) =>{
    let expense:Expense = req.body;
    try{
        expense = await expenseService.createExpense(expense);
        res.status(201);
        res.send(expense);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//get all expenses
app.get("/expenses", async (req,res) =>{
    let expenses:Expense[] = await expenseService.getAllExpenses();
    res.status(200);
    res.send(expenses);
});

//get expense by id
app.get("/expenses/:expenseId", async (req,res) =>{
    const expenseId:number = parseInt(req.params.expenseId);
    try{
        let expense:Expense = await expenseService.getExpense(expenseId);
        res.status(200);
        res.send(expense);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//update expense
app.put("/expenses/:expenseId", async (req,res) =>{
    const expenseId:number = parseInt(req.params.expenseId);
    let expense:Expense = req.body;
    expense.expenseId = expenseId;
    try{
        expense = await expenseService.updateExpense(expense);
        res.status(200);
        res.send(expense);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

//delete expense
app.delete("/expenses/:expenseId", async (req,res) =>{
    const expenseId:number = parseInt(req.params.expenseId);
    try{
        const deleted:boolean = await expenseService.deleteExpense(expenseId);
        res.status(200);
        res.send(deleted);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});
