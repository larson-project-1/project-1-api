export class Wedding{
    constructor(
        public weddingId:number, 
        public date:string,
        public location:string,
        public name:string,
        public budget:number
    ){}
}

export class Expense{
    constructor(
        public expenseId:number,
        public wId:number, //foregin key to wedding table
        public reason:string,
        public amount:number
    ){}
}