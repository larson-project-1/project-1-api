export class ResourceNotFoundError{
    constructor(
        public message:string
    ){}
}

export class IllegalDeleteError{
    constructor(
        public message:string
    ){}
}